<?php

function createproject($req, $p) {
  $sql = "INSERT INTO projects (title, `description`, client_id)
    VALUES (\"$p[title]\", \"$p[description]\", 0)";
  return $req->query($sql);
}

function createclient($req, $p) {
  $sql = "INSERT INTO clients (`name`, email_address)
    VALUES (\"$p[name]\", \"$p[email]\")";
  return $req->query($sql);
}

function createdeveloper($req, $p) {
  $sql = "INSERT INTO developers (firstname, lastname, level_id)
    VALUES (\"$p[firstname]\", \"$p[lastname]\", \"$p[level_id]\")";
  return $req->query($sql);
}

function read($req, $t) {
  $sql = "SELECT * FROM $t";
  return $req->query($sql);
}

function readproject($req) {
  $sql = "SELECT * FROM projects
  FULL JOIN clients USING (client_id)";
  return $req->query($sql);
}

function readdeveloper($req) {
  $sql = "SELECT * FROM developers
  JOIN levels USING (level_id)";
  return $req->query($sql);
}

function updateproject($req, $p) {
  $sql = "UPDATE projects
  SET title = \"$p[title]\", `description` = \"$p[description]\"
  WHERE project_id = $p[id]";
  return $req->query($sql);
}

function updateclient($req, $p) {
  $sql = "UPDATE clients
  SET `name` = \"$p[name]\", email_address = \"$p[email]\"
  WHERE client_id = $p[id]";
  return $req->query($sql);
}

function updatedeveloper($req, $p) {
  $sql = "UPDATE developers
  SET firstname = \"$p[firstname]\", `lastname` = \"$p[lastname]\", `level_id` = \"$p[level_id]\"
  WHERE developer_id = $p[id]";
  return $req->query($sql);
}

function deleteproject($req, $p) {
  $sql = "DELETE FROM projects
    WHERE project_id = $p[id]";
  return $req->query($sql);
}

function deleteclient($req, $p) {
  $sql = "DELETE FROM clients
    WHERE client_id = $p[id]";
  return $req->query($sql);
}

function deletedeveloper($req, $p) {
  $sql = "DELETE FROM developers
    WHERE developer_id = $p[id]";
  return $req->query($sql);
}

?>