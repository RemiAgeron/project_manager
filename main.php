<?php

if(isset($_POST)) {

//var_dump($_POST);

  switch (true) {

    case isset($_POST['crud']): ?>
      <div class=""></div>
      <?= $_POST['crud']($conn, $_POST) ? "L'opération a été un succès." : "L'opération a échoué."; ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['newproject']):
      projectform($_POST); ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['newclient']):
      clientform($_POST); ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['newdeveloper']):
      developerform($_POST); ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['showproject']):
      showproject($_POST);
      break;

    case isset($_POST['showclient']):
      showclient($_POST);
      break;

    case isset($_POST['showdeveloper']):
      showdeveloper($_POST);
      break;

    case isset($_POST['updateproject']):
      projectform($_POST); ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['updateclient']):
      clientform($_POST); ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['updatedeveloper']):
      developerform($_POST); ?>
      <a href="/public"><< Retour</a> <?php
      break;

    case isset($_POST['deleteproject']): ?>
      <div class="delete">
        <?= 'Voulez-vous vraiment supprimer ce projet ?'; ?>
        <form method="post">
          <input type="hidden" name="crud" value="deleteproject">
          <input type="hidden" name="id" value=<?= $_POST['id'] ?>>
          <input class="button" type="submit" value="Confirmer la suppression">
        </form>
      </div>
      <?php showproject($_POST);
      break;

    case isset($_POST['deleteclient']): ?>
      <div class="delete">
        <?= 'Voulez-vous vraiment supprimer ce client ?'; ?>
        <form method="post">
          <input type="hidden" name="crud" value="deleteclient">
          <input type="hidden" name="id" value=<?= $_POST['id'] ?>>
          <input class="button" type="submit" value="Confirmer la suppression">
        </form>
      </div>
      <?php showclient($_POST);
      break;

    case isset($_POST['deletedeveloper']): ?>
      <div class="delete">
        <?= 'Voulez-vous vraiment supprimer ce développeur ?'; ?>
        <form method="post">
          <input type="hidden" name="crud" value="deletedeveloper">
          <input type="hidden" name="id" value=<?= $_POST['id'] ?>>
          <input class="button" type="submit" value="Confirmer la suppression">
        </form>
      </div>
      <?php showdeveloper($_POST);
      break;

    default:
      require('table.php');
      break;
  }

} else require('table.php');

?>