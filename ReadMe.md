
L'application project manager permet la création, lecture, mise à jour et suppression dans une base de données dont les 3 grandes tables sont les clients, leurs projets et les développeurs qui peuvent les réaliser. 

Dans le dossier public il y a le MCD, la base de données au format SQL et le fichier index.php qui correspond au fichier principal de l'application.

Pour pouvoir accéder aux données il faut copier le fichier connection-exemple.php et le renommer connection.php, dans ce fichier il faut modifier les lignes 7 et 8 afin de rentrer votre mot de passe et nom d'utilisateur mysql.

L'application se repose sur un fichier principal (index.php) dans le dossier public sur lequel est appelé, à l'aide de fonction require, d'autres fichiers php pour conserver la mise en page.

Des fonctions ont été établies pour les requêtes SQL, les formulaires d'inscription, de modifications et de suppression.

Et des formulaires pour intéragir avec l'utilisateur.
