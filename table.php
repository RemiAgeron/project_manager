<form class="add" method="POST">
  <input class="button" type="submit" name="newproject" value="Ajouter un projet">
</form>

<table>
  <thead>
    <tr>
      <th>Projet</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach(readproject($conn) as $data) { ?>
      <tr>
        <td><?= $data['title'] ?></td>
        <td>
          <form method="post">
            <input type="hidden" name="id" value=<?= $data['project_id'] ?>>
            <input type="hidden" name="title" value="<?= $data['title'] ?>">
            <input type="hidden" name="description" value="<?= $data['description'] ?>">
            <input type="submit" name="showproject" value="Voir">
            <span>|</span>
            <input type="submit" name="updateproject" value="Modifier">
            <span>|</span>
            <input type="submit" name="deleteproject" value="Supprimer">
          </form>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>

<form class="add margtop15" method="POST">
  <input class="button" type="submit" name="newclient" value="Ajouter un client">
</form>

<table>
  <thead>
    <tr>
      <th>Client</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach(read($conn, 'clients') as $data) { ?>
      <tr>
        <td><?= $data['name'] ?></td>
        <td>
          <form method="post">
            <input type="hidden" name="id" value=<?= $data['client_id'] ?>>
            <input type="hidden" name="name" value="<?= $data['name'] ?>">
            <input type="hidden" name="email" value="<?= $data['email_address'] ?>">
            <input type="submit" name="showclient" value="Voir">
            <span>|</span>
            <input type="submit" name="updateclient" value="Modifier">
            <span>|</span>
            <input type="submit" name="deleteclient" value="Supprimer">
          </form>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>

<form class="add margtop15" method="POST">
  <input class="button" type="submit" name="newdeveloper" value="Ajouter un développeur">
</form>

<table>
  <thead>
    <tr>
      <th>Développeur</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach(readdeveloper($conn) as $data) { ?>
      <tr>
        <td><?= $data['firstname'] . " ".
            $data['lastname'] . " (" . $data['name'] . ")" ?></td>
        <td>
          <form method="post">
            <input type="hidden" name="id" value=<?= $data['developer_id'] ?>>
            <input type="hidden" name="firstname" value="<?= $data['firstname'] ?>">
            <input type="hidden" name="lastname" value="<?= $data['lastname'] ?>">
            <input type="hidden" name="level_id" value="<?= $data['level_id'] ?>">
            <input type="hidden" name="level" value="<?= $data['name'] ?>">
            <input type="submit" name="showdeveloper" value="Voir">
            <span>|</span>
            <input type="submit" name="updatedeveloper" value="Modifier">
            <span>|</span>
            <input type="submit" name="deletedeveloper" value="Supprimer">
          </form>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>