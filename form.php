<?php

function showproject($p) { ?>
  <div class="read">
    <h1><?= $p['title'] ?></h1>
    <p><?= $p['description'] ?></p>
    <a href="/public"><< Retour</a>
  </div>
<?php }

function showclient($p) { ?>
  <div class="read">
    <h1><?= $p['name']?></p>
    <h2><?= $p['email'] ?></h2>
    <a href="/public"><< Retour</a>
    </div>
    <?php }

function showdeveloper($p) { ?>
  <div class="read">
    <h1><?= $p['firstname'] . " " . $p['lastname'] . " : " . $p['level'] ?></h1>
    <a href="/public"><< Retour</a>
  </div>
<?php }

function projectform($p) { ?>
  <form class="form" method="POST">
    <input type="hidden" name="table" value="projects">
    <?php if(isset($p['id'])) { ?>
      <input type="hidden" name="crud" value="updateproject">
      <input type="hidden" name="id" value=<?= $p['id'] ?>>
    <?php } else { ?>
      <input type="hidden" name="crud" value="createproject">
    <?php } ?>
    <label for="title">Nom du projet</label>
    <input type="text" name="title" id="title" placeholder="Entrez le nom du projet" value="<?php
      if(isset($p['id'])) echo $p['title'];
    ?>">
    <label class="margtop15" for="description">Description</label>
    <textarea class="" name="description" id="description" rows="10" placeholder="Entrez la description du projet"><?php
      if(isset($p['id'])) echo $p['description'];
    ?></textarea>
    <input class="button margtop15" type="submit" value="Enregistrer">
  </form>

<?php }

function developerform($p) {
  require('connection.php') ?>
  <form class="form" method="POST">
    <div class="inline">
      <input type="hidden" name="table" value="developers">
      <?php if(isset($p['id'])) { ?>
        <input type="hidden" name="crud" value="updatedeveloper">
        <input type="hidden" name="id" value=<?= $p['id'] ?>>
      <?php } else { ?>
        <input type="hidden" name="crud" value="createdeveloper">
      <?php } ?>
      <label for="firstname">Prénom</label>
      <input type="text" name="firstname" id="firstname" placeholder="Entrez votre Prénom" value="<?php
        if(isset($p['id'])) echo $p['firstname'];
      ?>">
      <label class="margtop15" for="lastname">Nom</label>
      <input type="text" name="lastname" id="lastname" placeholder="Entrez votre Nom" value="<?php
        if(isset($p['id'])) echo $p['lastname'];
      ?>">
      <label class="margtop15" for="level">Niveau</label>
      <select name="level_id" id="level">
        <?php foreach (read($conn, 'levels') as $data) { ?>
          <option value=<?= $data['level_id'] ?>><?= $data['name'] ?></option>
        <?php } ?>
      </select>
    </div>
    <input class="button margtop15" type="submit" value="Enregistrer">
  </form>

<?php }

function clientform($p) { ?>
  <form class="form" method="POST">
    <div class="inline">
      <input type="hidden" name="table" value="clients">
      <?php if(isset($p['id'])) { ?>
        <input type="hidden" name="crud" value="updateclient">
        <input type="hidden" name="id" value=<?= $p['id'] ?>>
      <?php } else { ?>
        <input type="hidden" name="crud" value="createclient">
      <?php } ?>
      <label for="name">Nom du client</label>
      <input type="text" name="name" id="name" placeholder="Entrez votre nom ou entreprise" value="<?php
        if(isset($p['id'])) echo $p['name'];
      ?>">
      <label class="margtop15" for="email">Adresse</label>
      <input type="email" name="email" id="email" placeholder="Entrez l'adresse mail de contact" value="<?php
        if(isset($p['id'])) echo $p['email'];
      ?>">
    </div>
    <input class="button margtop15" type="submit" value="Enregistrer">
  </form>

<?php } ?>