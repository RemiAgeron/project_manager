<?php
  require('../connection.php');
  require('../crud.php');
  require('../form.php');
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="#">
    <link href="../style.css" rel="stylesheet">
    <title>Project Manager</title>
  </head>
<body>
  <header>
  <img src="https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/9ab5cd96-16ae-488d-803d-bf63779c5c2b.png" alt="https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/9ab5cd96-16ae-488d-803d-bf63779c5c2b.png" class="shrinkToFit transparent" width="80" height="80">
  <h1>Gestion de projets</h1>
  </header>
  <main>
    <?php require('../main.php') ?>
  </main>
  <footer>
    <h5>© Simplon.co</h5>
  </footer>
</body>
</html>